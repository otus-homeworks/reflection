﻿using System;
using System.Reflection;
using System.Text;


namespace Otus.Reflection
{
    public class MySerializerDeserializer<T>
    {
        public string ToCSV(T obj)
        {
            Type t = obj.GetType();

            FieldInfo[] classTFields = t.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            StringBuilder result = new StringBuilder(256);

            for (int i = 0; i < classTFields.Length; i++)
                result.Append(classTFields[i].Name + ":" + classTFields[i].GetValue(obj) + ";");

            return result.ToString();
        }


        public T FromCSV(string csv)
        {
            Type t = typeof(T);
            FieldInfo[] classTFields = t.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            ushort fieldsNum = (ushort)classTFields.Length;
            var result = Activator.CreateInstance(t);


            var NameValuePairs = csv.Split(";");
            if (NameValuePairs.Length <= fieldsNum)
                throw new InvalidOperationException("Число полей в десериализуемой строке, меньше чем полей в классе");

            for (ushort i = 0; i < fieldsNum; i++)
            {
                var NameValue = NameValuePairs[i].Split(":");
                var field = t.GetField(NameValue[0], BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

                field.SetValue(result, Convert.ChangeType(NameValue[1], field.FieldType));
            }

            return (T)result;
        }
    }
}
