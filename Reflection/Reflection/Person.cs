﻿namespace Otus.Reflection
{
    public class Person
    {
        // В значениях полей типа string не должно быть символов ':' и ';' - используются при сериализации/десериализации.
        // Десериализация сейчас только для типов string, ushort, int
        public string Name;
        public ushort Age;
        public int YearOfBirth;
        public int MonthOfBirth;
        public int DayOfBirth;
        private string PhoneNumber;

        // Чтобы можно было создать с помощью CreateInstance
        public Person() { }
        public Person(string aName, ushort aAge, int aYearOfBirth, int aMonthOfBirth, int aDayOfBirth, string aPhoneNumber)
        {
            Name = aName;
            Age = aAge;
            YearOfBirth = aYearOfBirth;
            MonthOfBirth = aMonthOfBirth;
            DayOfBirth = aDayOfBirth;
            PhoneNumber = aPhoneNumber;
        }
        public override string ToString()
        {
            return $"Name: {Name}   Age: {Age}  Birth:{DayOfBirth}.{MonthOfBirth}.{YearOfBirth}  PhoneNumber: {PhoneNumber} ";
        }
    }
}
