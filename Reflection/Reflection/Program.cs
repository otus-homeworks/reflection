﻿using System;
using System.Diagnostics;
using System.Text.Json;

namespace Otus.Reflection
{
    class Program
    {
        static string TimeElapsedToStr(TimeSpan ts)
        {
            return string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                            ts.Hours, ts.Minutes, ts.Seconds,
                            ts.Milliseconds / 10);
        }

        static void Main(string[] args)
        {
            try
            {
                Stopwatch stopWatchReflectionSerialize = new Stopwatch();
                Stopwatch stopWatchReflectionDeserialize = new Stopwatch();
                Stopwatch stopWatchJsonSerialize = new Stopwatch();
                Stopwatch stopWatchJsonDeserialize = new Stopwatch();

                var rnd = new Random();

                for (int i = 0; i < 1000000; i++)                
                {                   
                    Person prs = new Person("Некто", 2, 2000, rnd.Next(1, 12), rnd.Next(1, 28), "5-85-09");

                    // Сериализация/десериализация с рефлексией.
                    MySerializerDeserializer<Person> serDeser = new MySerializerDeserializer<Person>();
                    stopWatchReflectionSerialize.Start();
                    string serialized = serDeser.ToCSV(prs);
                    stopWatchReflectionSerialize.Stop();

                    stopWatchReflectionDeserialize.Start();
                    Person desrializedPrs = serDeser.FromCSV(serialized);
                    stopWatchReflectionDeserialize.Stop();

                    //Console.WriteLine("By reflection" + desrializedPrs.GetInfo());

                    // Сериализация/десериализация с json.
                    stopWatchJsonSerialize.Start();
                    string serializedByJson = JsonSerializer.Serialize(prs);
                    stopWatchJsonSerialize.Stop();

                    stopWatchJsonDeserialize.Start();
                    Person desrializedByJsonPrs = JsonSerializer.Deserialize<Person>(serializedByJson);
                    stopWatchJsonDeserialize.Stop();

                    if( i % 5000 == 0 ) 
                        Console.Write($"i={i} ");
                }

                Console.WriteLine();
                Console.WriteLine("Reflection serialize time: " + TimeElapsedToStr(stopWatchReflectionSerialize.Elapsed));
                Console.WriteLine("Reflection deserialize time: " + TimeElapsedToStr(stopWatchReflectionDeserialize.Elapsed));
                Console.WriteLine("Json serialize time: " + TimeElapsedToStr(stopWatchJsonSerialize.Elapsed));
                Console.WriteLine("Json deserialize time: " + TimeElapsedToStr(stopWatchJsonDeserialize.Elapsed));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
