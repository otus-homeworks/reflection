using System;
using Xunit;

namespace Otus.Reflection
{
    internal class ForTestWithAnotherClass
    {
        public string Name;        
        public uint Anything;

        // ��� ����� ����� ���������� ��������������.
        public ForTestWithAnotherClass()
        {
            Name = "qwerty";
            Anything = 56;
        }
    }

    public class SerializeDeserializeTest
    {
        [Fact]
        public void TestForClassWithSupportedTypes()
        {
            // Arrange   
            Person prs = new Person("�����", 21, 2000, 5, 23, "5-85-09");            
            Person prs2 = new Person();

            string strAboutPrs = prs.ToString();
            string strAboutPrs2 = prs2.ToString();
            MySerializerDeserializer<Person> serDeser = new MySerializerDeserializer<Person>();

            // Act
            string strSerializedPrs = serDeser.ToCSV(prs);
            Person deserializedPrs = serDeser.FromCSV(strSerializedPrs);
            

            // Assert 
            Assert.NotEqual(strAboutPrs, strAboutPrs2);
            Assert.NotNull(deserializedPrs);
            Assert.Equal(strAboutPrs, deserializedPrs.ToString());
        }

        [Fact]
        public void TestForClassWithUnsupportedTypes()
        {
            // Arrange   
            ForTestWithAnotherClass forTest = new ForTestWithAnotherClass();
            MySerializerDeserializer<ForTestWithAnotherClass> serDeser = new MySerializerDeserializer<ForTestWithAnotherClass>();

            // Act
            string strSerialized = serDeser.ToCSV(forTest);
            ForTestWithAnotherClass deserialized = serDeser.FromCSV(strSerialized);

            // Assert 
            Assert.Equal("qwerty", deserialized.Name);
            Assert.True(deserialized.Anything == 56);            
        }

    }
}
